# KiCad libraries #

## Configuration ##

Since it´s not possible to refer 3D models from footprits with a relative path, they have been located using the enviroment variable **MYLYB** which must be set to this folder location.

## Footprints ##

### Resistors ###
- Pot-PTV09A-4

### Connectors ###
- Connector-CNC_Tech-51125-02-0200-01
- Connector-TE-1744048-2
- [IO-XLR3-F-EH](https://www.digikey.es/product-detail/es/io-audio-technologies/IO-XLR3-F-EH/1937-1146-ND/9931888)
- [IO-XLR3-M-EH](https://www.digikey.es/product-detail/es/io-audio-technologies/IO-XLR3-M-EH/1937-1145-ND/9931887)
- [Jack-3.5mm-SJ1_3555NG](https://www.digikey.es/product-detail/es/cui-devices/SJ1-3555NG/CP1-3555NG-ND/738708)
- USB_C_10164359-00011LF
  
### Switches ###
- Switch-50-0118-00
- Switch-PB400OAQX
- Switch-TL3305C
- Switch-TL3336

### Battery ###
- Battery-SolderedAA
- Battery-Pads

### Others ###
- [Crystal CSM-7X](https://www.digikey.es/product-detail/es/ecs-inc/ECS-122-8-18-5PLX-AGN-TR/XC2786CT-ND/8619662)
- Display-SSD1306-Module-7_28
- Adafruit VS1053
- [Connector-AC-IEC_320_C8-Schurter_2570_2210](https://www.digikey.es/es/products/detail/schurter-inc/4300-0104/8299835)
- Diode-DO-219AD
- [Fuse_6.1_2.54](https://www.digikey.es/es/products/detail/bel-fuse-inc/0679H4000-05/6139772)


## Symbols ##
- D_TVS_UNI
- SI4435FDY-T1-GE3
- ISO7721
- THVD1419DR
- vs1053b
- Adafruit_VS1053
- BQ21040
- [OP-MCP6006U](https://www.digikey.es/en/products/detail/microchip-technology/MCP6006UT-E-OT/13985884)
- [OP-ADA4522-1](https://www.digikey.es/en/products/detail/analog-devices-inc/ADA4522-1ARZ-R7/6008921)
- [NOR-SN74LVC1G02DCKR](instruments/SN74LVC1G02DCKR/385715)
